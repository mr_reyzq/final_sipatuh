package com.kemenag.sipatuh.SuperAdmin;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kemenag.sipatuh.R;

import java.util.List;

public class AdapterDashboard2 extends RecyclerView.Adapter<AdapterDashboard2.ViewHolder>{

    private Context context;
    private List<Model2> list;

    public AdapterDashboard2(Context context,List<Model2> list){
        this.context=context;
        this.list=list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,int viewType){
        View v=LayoutInflater.from(context).inflate(R.layout.list_model_posisi_paket,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder,int position){
        Model2 model = list.get(position);

        holder.nama_pihk.setText(model.getNama_pihk());
        holder.kode_pihk.setText(model.getKode_pihk());
        holder.code_paket.setText(model.getCode_paket());
        holder.nm_paket.setText(model.getNm_paket());
        holder.pemberangkatan_ke.setText("Pemberangkatan Ke " + model.getPemberangkatan_ke());
        holder.jumlah_jamaah.setText(model.getJumlah_jamaah() + " Jemaah");
        holder.posisi_jamaah.setText(model.getPosisi_jamaah());

    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nama_pihk, kode_pihk, code_paket, nm_paket, pemberangkatan_ke, jumlah_jamaah, posisi_jamaah;

        public ViewHolder(View itemView) {
            super(itemView);

            nama_pihk = itemView.findViewById(R.id.nm_pihk);
            kode_pihk = itemView.findViewById(R.id.nmr_pihk);
            code_paket = itemView.findViewById(R.id.nmr_paket);
            nm_paket = itemView.findViewById(R.id.nm_paket);
            pemberangkatan_ke = itemView.findViewById(R.id.no_berangkat);
            jumlah_jamaah = itemView.findViewById(R.id.jml_jamaah);
            posisi_jamaah = itemView.findViewById(R.id.posisi);
        }
    }

}
