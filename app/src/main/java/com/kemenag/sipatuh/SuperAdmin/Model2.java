package com.kemenag.sipatuh.SuperAdmin;

public class Model2 {

    public String nama_pihk, kode_pihk, code_paket, nm_paket, pemberangkatan_ke, jumlah_jamaah, posisi_jamaah;

    public Model2() {
    }

    public Model2(String nama_pihk, String kode_pihk, String code_paket, String nm_paket,
                  String pemberangkatan_ke, String jumlah_jamaah, String posisi_jamah) {
        this.nama_pihk = nama_pihk;
        this.kode_pihk = kode_pihk;
        this.code_paket = code_paket;
        this.nm_paket = nm_paket;
        this.pemberangkatan_ke = pemberangkatan_ke;
        this.jumlah_jamaah = jumlah_jamaah;
        this.posisi_jamaah = posisi_jamah;
    }

    public String getNama_pihk() {
        return nama_pihk;
    }

    public void setNama_pihk(String nama_pihk) {
        this.nama_pihk = nama_pihk;
    }

    public String getKode_pihk() {
        return kode_pihk;
    }

    public void setKode_pihk(String kode_pihk) {
        this.kode_pihk = kode_pihk;
    }

    public String getCode_paket() {
        return code_paket;
    }

    public void setCode_paket(String code_paket) {
        this.code_paket = code_paket;
    }

    public String getNm_paket() {
        return nm_paket;
    }

    public void setNm_paket(String nm_paket) {
        this.nm_paket = nm_paket;
    }

    public String getPemberangkatan_ke() {
        return pemberangkatan_ke;
    }

    public void setPemberangkatan_ke(String pemberangkatan_ke) {
        this.pemberangkatan_ke = pemberangkatan_ke;
    }

    public String getJumlah_jamaah() {
        return jumlah_jamaah;
    }

    public void setJumlah_jamaah(String jumlah_jamaah) {
        this.jumlah_jamaah = jumlah_jamaah;
    }

    public String getPosisi_jamaah() {
        return posisi_jamaah;
    }

    public void setPosisi_jamaah(String posisi_jamaah) {
        this.posisi_jamaah = posisi_jamaah;
    }
}