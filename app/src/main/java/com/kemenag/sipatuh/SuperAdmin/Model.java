package com.kemenag.sipatuh.SuperAdmin;

public class Model {

    public String nama, jenis, no_telp, nmr_porsi, status_lokasi;

    public Model() {
    }

    public Model(String nama, String jenis, String no_telp, String nmr_porsi, String status_lokasi) {
        this.nama = nama;
        this.jenis = jenis;
        this.no_telp = no_telp;
        this.nmr_porsi = nmr_porsi;
        this.status_lokasi = status_lokasi;
    }


    public String getNama() {
        return nama;
    }

    public String getJenis() {
        return jenis;
    }

    public String getNo_telp() {
        return no_telp;
    }

    public String getNmr_porsi() {
        return nmr_porsi;
    }

    public String getStatus_lokasi() {
        return status_lokasi;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    public void setNmr_porsi(String nmr_porsi) {
        this.nmr_porsi = nmr_porsi;
    }

    public void setStatus_lokasi(String status_lokasi) {
        this.status_lokasi = status_lokasi;
    }
}