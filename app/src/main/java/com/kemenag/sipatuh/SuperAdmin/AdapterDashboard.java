package com.kemenag.sipatuh.SuperAdmin;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kemenag.sipatuh.R;

import java.util.List;

public class AdapterDashboard extends RecyclerView.Adapter<AdapterDashboard.ViewHolder>{

            private Context context;
            private List<Model> list;

            public AdapterDashboard(Context context,List<Model> list){
                    this.context=context;
                    this.list=list;
                    }

            @Override
            public ViewHolder onCreateViewHolder(ViewGroup parent,int viewType){
                    View v=LayoutInflater.from(context).inflate(R.layout.list_model_posisi_jamaah,parent,false);
                    return new ViewHolder(v);
                    }

            @Override
            public void onBindViewHolder(ViewHolder holder,int position){
                    Model model = list.get(position);

                    holder.nama.setText(model.getNama());
                    holder.jenis.setText(model.getJenis());
                    holder.nmr_porsi.setText(model.getNmr_porsi());
                    holder.no_telp.setText(model.getNo_telp());
                    holder.status_lokasi.setText(model.getStatus_lokasi());

                    }

            @Override
            public int getItemCount(){
                    return list.size();
                    }

            public class ViewHolder extends RecyclerView.ViewHolder {
                public TextView nama, jenis, no_telp, nmr_porsi, status_lokasi;

                public ViewHolder(View itemView) {
                    super(itemView);

                    nama = itemView.findViewById(R.id.nm_jemaah);
                    jenis = itemView.findViewById(R.id.nm_jenis);
                    no_telp = itemView.findViewById(R.id.nmr_hp);
                    nmr_porsi = itemView.findViewById(R.id.nmr_porsi);
                    status_lokasi = itemView.findViewById(R.id.posisi);
                }
            }

}
