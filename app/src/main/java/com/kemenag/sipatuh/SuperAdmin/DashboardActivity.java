package com.kemenag.sipatuh.SuperAdmin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kemenag.sipatuh.BuildConfig;
import com.kemenag.sipatuh.LoginPackage.LoginActivity;
import com.kemenag.sipatuh.LoginPackage.Preferences;
import com.kemenag.sipatuh.PublicPackage.AppVar;
import com.kemenag.sipatuh.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

public class DashboardActivity extends AppCompatActivity {

    SwipeRefreshLayout pullToRefresh;

    RecyclerView mList, mList2;
    LinearLayoutManager linearLayoutManager;
    DividerItemDecoration dividerItemDecoration;
    List<Model> modelList;
    RecyclerView.Adapter adapter;

    List<Model2> modelList2;
    LinearLayoutManager linearLayoutManager2;
    DividerItemDecoration dividerItemDecoration2;
    RecyclerView.Adapter adapter2;

    PieChartView pieChartView, pieChartView2;
    List<SliceValue> pieData, pieData2;

    TextView ket1, ket2, ket3, ket4, ket5, ket6, ket7, ket8, ket9, ket10, ket11;
    TextView ang_stats1, ang_stats2, ang_stats3, ang_stats4, ang_stats5, ang_stats6;
    Button refresh_paket, refresh_jamaah;
    EditText numb_paket, numb_jamaah;

    String DASH_BER_TANAH_AIR = AppVar.BASE_URL + "API/Dashboard/countBerangkatTanahAir";
    String DASH_MADINAH = AppVar.BASE_URL + "API/Dashboard/countMadinah";
    String DASH_JEDDAH = AppVar.BASE_URL + "API/Dashboard/countJeddah";
    String DASH_MEKKAH = AppVar.BASE_URL + "API/Dashboard/countMekkah";
    String DASH_TARWIYAH = AppVar.BASE_URL + "API/Dashboard/countTarwiyah";
    String DASH_KED_ARAFAH = AppVar.BASE_URL + "API/Dashboard/countArafah";
    String DASH_KEP_MINA = AppVar.BASE_URL + "API/Dashboard/countMina";
    String DASH_KEP_ARAB = AppVar.BASE_URL + "API/Dashboard/countPulangas";
    String DASH_KEP_TANAH_AIR = AppVar.BASE_URL + "API/Dashboard/countPulang";
    String DASH_ALL_BERANGKAT = AppVar.BASE_URL + "API/Dashboard/countBerangkat";
    String DASH_ALL_BLM_BERANGKAT = AppVar.BASE_URL + "API/Dashboard/countBelumBerangkat";
    String DASH_PULANG_TA = AppVar.BASE_URL + "API/Dashboard/countPulang";
    String DASH_SAKIT = AppVar.BASE_URL + "API/Dashboard/countSakit";
    String DASH_WAFAT = AppVar.BASE_URL + "API/Dashboard/countWafat";
    String DASH_TIBA_MADINAH = AppVar.BASE_URL + "API/Dashboard/getTibaMadinah";
    String DASH_TIBA_JEDDAH = AppVar.BASE_URL + "API/Dashboard/getTibaJeddah";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        pullToRefresh = findViewById(R.id.pullToRefresh);

        //RECYLERVIEW DATA JAMAAH
        mList2 = findViewById(R.id.recyclerview1);
        modelList2 = new ArrayList<>();
        adapter2 = new AdapterDashboard2(getApplicationContext(),modelList2);
        linearLayoutManager2 = new LinearLayoutManager(this);
        linearLayoutManager2.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration2 = new DividerItemDecoration(mList2.getContext(), linearLayoutManager2.getOrientation());
        mList2.setHasFixedSize(true);
        mList2.setLayoutManager(linearLayoutManager2);
        mList2.addItemDecoration(dividerItemDecoration2);
        mList2.setAdapter(adapter2);

        //RECYCLERVIEW DATA PAKET
        mList = findViewById(R.id.recylerview2);
        modelList = new ArrayList<>();
        adapter = new AdapterDashboard(getApplicationContext(),modelList);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());
        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(adapter);

        pieChartView = findViewById(R.id.chart);
        pieData = new ArrayList<>();

        pieChartView2 = findViewById(R.id.chart2);
        pieData2 = new ArrayList<>();

        numb_paket = findViewById(R.id.cari_paket);
        numb_jamaah = findViewById(R.id.cari_jamaah);

//        previous1 = findViewById(R.id.previous1);
//        previous2 = findViewById(R.id.previous2);
//        next1 = findViewById(R.id.next1);
//        next2 = findViewById(R.id.next2);

        refresh_paket = findViewById(R.id.btn_cari_paket);
        refresh_jamaah =  findViewById(R.id.btn_cari_jamaah);

        ang_stats1 = findViewById(R.id.angka_status);
        ang_stats2 = findViewById(R.id.angka_status2);
        ang_stats3 = findViewById(R.id.angka_status3);
        ang_stats4 = findViewById(R.id.angka_status4);
        ang_stats5 = findViewById(R.id.angka_status5);
        ang_stats6 = findViewById(R.id.angka_status6);

        ket1 = findViewById(R.id.hsl_bertanahair);
        ket2 = findViewById(R.id.hsl_madinah);
        ket3 = findViewById(R.id.hsl_jeddah);
        ket4 = findViewById(R.id.hsl_mekkah);
        ket5 = findViewById(R.id.hsl_tarwiyah);
        ket6 = findViewById(R.id.hsl_arafah);
        ket7 = findViewById(R.id.hsl_mina);
        ket8 = findViewById(R.id.hsl_keparab);
        ket9 = findViewById(R.id.hsl_keptanahair);

        ket10 = findViewById(R.id.hsl_sdhberangkat);
        ket11 = findViewById(R.id.hsl_blmberangkat);

        LoadDataUrut();
        LoadDataUrut2();
        LoadDataUrut3();
        LoadDataUrut4();
        LoadDataUrut5();
        LoadDataUrut6();
        LoadDataUrut7();
        LoadDataUrut8();
        LoadDataUrut9();
        LoadDataUrut10();
        LoadDataUrut11();
        LoadDataUrut12();
        LoadDataUrut13();
        LoadDataUrut14();
        LoadDataUrut15();
        LoadDataUrut16();
        getData();
        getData2();
        test();

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                LoadDataUrut();
                LoadDataUrut2();
                LoadDataUrut3();
                LoadDataUrut4();
                LoadDataUrut5();
                LoadDataUrut6();
                LoadDataUrut7();
                LoadDataUrut8();
                LoadDataUrut9();
                LoadDataUrut10();
                LoadDataUrut11();
                LoadDataUrut12();
                LoadDataUrut13();
                LoadDataUrut14();
                LoadDataUrut15();
                LoadDataUrut16();
                getData();
                getData2();
                pieData.clear();
                pieData2.clear();
                test();
                pullToRefresh.setRefreshing(false);
            }
        });

        refresh_paket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData ();
                clearData2();
            }
        });

        refresh_jamaah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData2();
                clearData();
            }
        });

    }

    public void clearData() {
        modelList.clear();
        adapter.notifyDataSetChanged();
    }

    public void clearData2() {
        modelList2.clear();
        adapter2.notifyDataSetChanged();
    }

    private void getData() {
        final String dapat = numb_paket.getText().toString();
        String url = AppVar.BASE_URL + "API/Dashboard/getBerangkat";
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("start", dapat);
            jsonParams.put("draw", 1);
            jsonParams.put("length", 10);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.POST, url, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            JSONArray dataJamaah = response.getJSONArray("data");

                            for (int i = 0; i < dataJamaah.length(); i++) {
                                JSONObject userData = dataJamaah.getJSONObject(i);

                                Model2 model = new Model2();
                                model.setNama_pihk(userData.getString("pihk"));
                                model.setKode_pihk(userData.getString("kd_pihk"));
                                model.setNm_paket(userData.getString("nama_paket"));
                                model.setCode_paket(userData.getString("kd_paket"));
                                model.setPemberangkatan_ke(userData.getString("pemberangkatan_ke"));
                                model.setPosisi_jamaah(userData.getString("posisi"));
                                model.setJumlah_jamaah(userData.getString("jumlah"));

                                modelList2.add(model);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }

                        adapter2.notifyDataSetChanged();
                        progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
                progressDialog.dismiss();
            }

        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    private void getData2() {
        final String dapat = numb_jamaah.getText().toString();
        String url = AppVar.BASE_URL + "API/Dashboard/getPosisiJamaah";
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("start", dapat);
            jsonParams.put("draw", 1);
            jsonParams.put("length", 10);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.POST, url, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    try {

                        JSONArray dataJamaah = response.getJSONArray("data");

                        for (int i = 0; i < dataJamaah.length(); i++) {
                            JSONObject userData = dataJamaah.getJSONObject(i);

                            Model model = new Model();
                            model.setNama(userData.getString("nama_jamaah"));
                            model.setJenis(userData.getString("jenis_jamaah"));
                            model.setNmr_porsi(userData.getString("kd_porsi"));
                            model.setNo_telp(userData.getString("nomor_hp"));
                            model.setStatus_lokasi(userData.getString("posisi"));

                            modelList.add(model);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }

                adapter.notifyDataSetChanged();
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
                progressDialog.dismiss();
            }

        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    private void test() {
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Checking your version");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppVar.MIN_VERSION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            String real_versi = obj.getJSONObject(0).optString("description");
                            double current_version = Double.valueOf(BuildConfig.VERSION);
                            double minimum_version = Double.valueOf(real_versi);
                            if(current_version < minimum_version){
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.kemenag.sipatuh&hl=in")));
                                Toast.makeText(DashboardActivity.this, "Aplikasi anda kadaluarsa, silahkan update aplikasi anda", Toast.LENGTH_LONG).show();
                                finish();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_BER_TANAH_AIR,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            ket1.setText(String.valueOf(dataJamaah2));
                            pieData.add(new SliceValue(dataJamaah2, getResources().getColor(R.color.ket_abu)));

                            PieChartData pieChartData = new PieChartData(pieData).setHasCenterCircle(true);
                            pieChartView.setPieChartData(pieChartData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut2() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_TIBA_MADINAH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            ang_stats5.setText(String.valueOf(dataJamaah2));
                            ket2.setText(String.valueOf(dataJamaah2));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut3() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_MEKKAH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            pieData.add(new SliceValue(dataJamaah2, getResources().getColor(R.color.ket_birugelap)));
                            ket4.setText(String.valueOf(dataJamaah2));

                            PieChartData pieChartData = new PieChartData(pieData).setHasCenterCircle(true);
                            pieChartView.setPieChartData(pieChartData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut4() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_TIBA_JEDDAH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            ang_stats6.setText(String.valueOf(dataJamaah2));
                            ket3.setText(String.valueOf(dataJamaah2));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut5() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_TARWIYAH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            pieData.add(new SliceValue(dataJamaah2, getResources().getColor(R.color.ket_coklat)));
                            ket5.setText(String.valueOf(dataJamaah2));

                            PieChartData pieChartData = new PieChartData(pieData).setHasCenterCircle(true);
                            pieChartView.setPieChartData(pieChartData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut6() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_KED_ARAFAH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            pieData.add(new SliceValue(dataJamaah2, getResources().getColor(R.color.ket_maroon)));
                            ket6.setText(String.valueOf(dataJamaah2));

                            PieChartData pieChartData = new PieChartData(pieData).setHasCenterCircle(true);
                            pieChartView.setPieChartData(pieChartData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut7() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_KEP_MINA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            pieData.add(new SliceValue(dataJamaah2, getResources().getColor(R.color.ket_merah)));
                            ket7.setText(String.valueOf(dataJamaah2));

                            PieChartData pieChartData = new PieChartData(pieData).setHasCenterCircle(true);
                            pieChartView.setPieChartData(pieChartData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut8() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_KEP_ARAB,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            pieData.add(new SliceValue(dataJamaah2, getResources().getColor(R.color.ket_ungu)));
                            ket8.setText(String.valueOf(dataJamaah2));

                            PieChartData pieChartData = new PieChartData(pieData).setHasCenterCircle(true);
                            pieChartView.setPieChartData(pieChartData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut9() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_KEP_TANAH_AIR,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            pieData.add(new SliceValue(dataJamaah2, getResources().getColor(R.color.ket_orens)));
                            ket9.setText(String.valueOf(dataJamaah2));

                            PieChartData pieChartData = new PieChartData(pieData).setHasCenterCircle(true).setHasCenterCircle(true);
                            pieChartView.setPieChartData(pieChartData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut10() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_ALL_BERANGKAT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            pieData2.add(new SliceValue(dataJamaah2, getResources().getColor(R.color.ket_birugelap)));
                            ket10.setText(String.valueOf(dataJamaah2));
                            ang_stats1.setText(String.valueOf(dataJamaah2));

                            PieChartData pieChartData = new PieChartData(pieData2).setHasCenterCircle(true).setHasCenterCircle(true);
                            pieChartView2.setPieChartData(pieChartData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut11() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_ALL_BLM_BERANGKAT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            pieData2.add(new SliceValue(dataJamaah2, getResources().getColor(R.color.ket_hijau)));
                            ket11.setText(String.valueOf(dataJamaah2));

                            PieChartData pieChartData = new PieChartData(pieData2).setHasCenterCircle(true).setHasCenterCircle(true);
                            pieChartView2.setPieChartData(pieChartData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut12() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_SAKIT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            ang_stats3.setText(String.valueOf(dataJamaah2));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut13() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_WAFAT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int number = obj.length();
                            ang_stats4.setText(String.valueOf(number));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut14() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_SAKIT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            pieData.add(new SliceValue(dataJamaah2, getResources().getColor(R.color.ket_hijau)));

                            PieChartData pieChartData = new PieChartData(pieData).setHasCenterCircle(true);
                            pieChartView.setPieChartData(pieChartData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut15() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_SAKIT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            pieData.add(new SliceValue(dataJamaah2, getResources().getColor(R.color.ket_birulaut)));
                            ket3.setText(String.valueOf(dataJamaah2));

                            PieChartData pieChartData = new PieChartData(pieData).setHasCenterCircle(true);
                            pieChartView.setPieChartData(pieChartData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadDataUrut16() {

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DASH_PULANG_TA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pDialog.dismiss();

                        try {

                            JSONArray obj = new JSONArray(response);
                            int dataJamaah2 = obj.getJSONObject(0).optInt("jumlah");

                            ang_stats2.setText(String.valueOf(dataJamaah2));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                Map<String, String> headers = new HashMap<>();
                String credentials = "admin:1234";
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_act, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.logout){
            Preferences.clearLoggedInUser(getBaseContext());
            startActivity(new Intent(this, LoginActivity.class));
            Toast.makeText(this, "Anda Berhasil Logout", Toast.LENGTH_SHORT).show();
            finish();
        }
        return true;
    }

}
